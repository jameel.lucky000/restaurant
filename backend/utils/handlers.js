import {Restaurant} from "./model.js"

function post_resturant_handler(req,res){
    const {restaurantName,managerName,contactNumber,menu,timings} = req.body


const restaurant_data = {
    restaurantName,
    managerName,
    contactNumber,
    menu,
    timings
};

const restaurants = new Restaurant(restaurant_data)

Restaurant.find({restaurantName:restaurantName},(err,data)=>{
    if(err){
        return res.status(406).send({message:"connot connect to the database "})
    }
    if(data.length!==0){
        if(data[0].restaurantName === restaurantName){
            return res.status(402).send(JSON.stringify({message:"Restaurant name already taken please try another name"}))
        }
    }
    else if(data.length==0){
        restaurants.save().then(()=>{
            return res.status(200).send({message : "Resturant added"})
        })
    } 
})
}

 function get_restaurant_handler (req,res){
    Restaurant.find({}).then((restaurants)=>{
        res.status(200).send(restaurants)
    }).catch((err)=>{
        res.status(402).send({message:err})
    })
    }


function edit_restaurant_handler(req,res){
    const {restaurantName,managerName,contactNumber,menu,timings} = req.body;
    Restaurant.findOne({restaurantName:restaurantName},(err,data)=>{
        if(err){
            res.status(404).send({message : "Error finding in database"})
        }
        else{
            if(data){
                Restaurant.updateOne({restaurantName:restaurantName},{restaurantName:restaurantName,managerName:managerName,contactNumber:contactNumber,menu:menu,timings:timings})
                .then(()=>{
                    res.status(200).send({message:"Edited successfully"})
                })
                .catch(()=>{
                    res.status(304).send({message:"unable to update database"})
                })
            }
            else{
                res.status(401).send({message:"Resturant not found"})
            }
        }
    })
};

function delete_restaurant_handler(req,res){
    const {restaurantName} = req.body
    Restaurant.findOneAndRemove({restaurantName:restaurantName},(err,deleted)=>{
        if(err){
            res.send(402).send({message:"Error in removing"})
        }
        else if(!err){
            res.send({message:"Removed successfully"})
        }
    })
}

export {post_resturant_handler,get_restaurant_handler,edit_restaurant_handler,delete_restaurant_handler};