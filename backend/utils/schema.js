import mongoose from "mongoose";

const ResturantData = new mongoose.Schema({
    restaurantName : {type:String,required:true},
    managerName : {type:String,required:true},
    contactNumber : {type:String,required:true},
    menu : [],
    timings : {type:Number,required:true}
})

export default ResturantData