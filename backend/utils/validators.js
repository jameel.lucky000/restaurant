import {_name_helper,_phoneNo_helper,_time_helper} from "./helper.js"
/**
 * 
 * @param {} req takes the inputs from the user
 * @param {*} res gives back the response to the user
 * @param {*} next calls the next functoin 
 * @returns response
 */


function keys_validator(req, res, next) {
    const keys = req.body;
    const keylist = [];
    for (const [key, value] of Object.entries(keys)) {
      if (
        !(
          key.trim().toLowerCase() == "restaurantname" ||
          key.trim().toLowerCase() == "managername"||
          key.trim().toLowerCase() == "contactnumber"||
          key.trim().toLowerCase() == "menu"||
          key.trim().toLowerCase() == "timings"
        )
      ) {
        return res
          .status(406)
          .send({message:`Please provide the exact key. The key:${key} is incorrect`});
      }
      if (keylist.includes(key.toLowerCase())) {
      return res.status(406).send({message:`key naming:${key} is reapeting`});
      }
      keylist.push(key.toLowerCase());
    }
    if (keylist.length < 5) {
      return res.status(406).send({message:`${5 - keylist.length} key missing`});
    }
    if (keylist.length > 5) {
      return res.status(406).send({message:`${keylist.length - 5} key extra`});
    }
    next();
  };

  function values_validator(req,res,next){
    const { restaurantName,managerName,contactNumber,menu,timings } = req.body;
    const error_object = {};
  
    error_object.restaurantName = _name_helper(restaurantName).name;
    error_object.managerName = _name_helper(managerName).name;
    error_object.contactNumber = _phoneNo_helper(contactNumber).phoneNo;
    error_object.time = _time_helper(timings).time;

  for (const [key, value] of Object.entries(error_object)) {
    if (value) {
      return res.status(200).send({message:error_object});
    }
  }
    next();
  }
  
  export { keys_validator,values_validator };
  