
//name validators
const _name_helper = (name) => {
    const returnObject = {};
  
    if (typeof name != "string") {
      returnObject.name = "name should be of string type";
      return returnObject;
    }
  
    if (name.length < 3) {
      returnObject.name = "name length should be at least 4 characters long";
      return returnObject;
    }
  
    if (name.length > 25) {
      returnObject.name = "name length shouldn't be 25 characters long";
      return returnObject;
    }
  
    if (!name.match(/^[a-zA-Z ]+$/)) {
      returnObject.name = "name should be of the correct format";
      return returnObject;
    }
  
    return returnObject;
  };

  //time validators in 24 hours format
  const _time_helper = (time)=>{
      const returnObject = {};
      if(typeof time != "number"){
          returnObject.time = "time should be of number type and in 24 hours format only";
          return returnObject
      }
      else if(time>24||time<0){
          returnObject.time = "time should be in 24 hours format only"
      }
      return returnObject
  }


//contactNumber validators
const _phoneNo_helper = (phoneNo) => {
    const returnObject = {};
  
    if (typeof phoneNo != "number") {
      returnObject.phoneNo = "phone number should be of number type";
      return returnObject;
    }
  
    if (phoneNo.toString().length < 10) {
      returnObject.phoneNo =
        "phone number length should be at least 10 characters long";
      return returnObject;
    }
  
    if (phoneNo.length > 10) {
      returnObject.phoneNo = "phone number length should be a max of 10";
      return returnObject;
    }
    return returnObject;
  };
  
  export {_name_helper,_phoneNo_helper,_time_helper}