import express from "express";
import mongoose from "mongoose";
import "dotenv/config";
import bodyParser from "body-parser";
import cors from "cors";
import { keys_validator, values_validator } from "./utils/validators.js";
import { delete_restaurant_handler, edit_restaurant_handler, get_restaurant_handler, post_resturant_handler } from "./utils/handlers.js";

//creating the app instance
const app = express();
app.use(bodyParser.json());
// Returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));
//initiating cors for resource sharing
app.use(cors({ origin: "*" }));

//---------------------------------------------------post call-------------------------------------------
app.post("/add",keys_validator,values_validator,post_resturant_handler)
//-----------------------------------------------get call-----------------------------------------
app.get("/get",get_restaurant_handler)
//---------------------------------------------put call-----------------------------------
app.put("/edit",edit_restaurant_handler)
//-------------------------------delete call----------------------------------------
app.delete("/delete",delete_restaurant_handler)

//connecting the bakend server to the database and listing at the port
mongoose
    .connect(process.env.MONGO_URL)
    .then(() => {
        app.listen(process.env.PORT, () => {
            console.log(`Server running at port:${process.env.PORT}...`);
        });
    })
    .catch((err) => {
        console.log(`cannot connect to the database due to ${err}`);
    });
